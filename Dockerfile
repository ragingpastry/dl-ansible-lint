ARG yamllint_version
ARG os_version

FROM quay.io/bitnami/python:3.8-prod-debian-10

COPY requirements.txt /tmp/requirements.txt
COPY .yamllint /.yamllint
COPY .ansible-lint.yml /.ansible-lint.yml

RUN \
python3 -m pip install \
${PIP_INSTALL_ARGS} -r /tmp/requirements.txt && \
rm -rf /root/.cache

ENV SHELL /bin/bash
